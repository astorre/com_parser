require 'sidekiq/web'

Rails.application.routes.draw do

  resources :tasks
  resources :domains

  get 'fill_up_titles' => 'tasks#fill_up_titles'
  get 'fill_up_domains' => 'domains#fill_up_domains'
  get 'add_nums' => 'domains#add_nums'

  mount Sidekiq::Web => '/sidekiq'

end
