Sidekiq.configure_server do |config|
  config.redis = { namespace: 'com_parser', db: 6, password: Rails.application.secrets.redis_pas }
end

Sidekiq.configure_client do |config|
  config.redis = { namespace: 'com_parser', db: 6, password: Rails.application.secrets.redis_pas }
end
