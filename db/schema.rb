# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160808124133) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "domains", force: :cascade do |t|
    t.string   "domain"
    t.integer  "domain_length"
    t.boolean  "consonants_m3"
    t.integer  "url_len_med"
    t.integer  "title_len_med"
    t.integer  "slashes_med"
    t.integer  "req_dom_pgs"
    t.string   "req_dom_url"
    t.integer  "direct_phones"
    t.boolean  "has800"
    t.integer  "total_phones"
    t.boolean  "has_about_page"
    t.boolean  "has_contact_page"
    t.boolean  "has_social"
    t.integer  "commun_methods"
    t.boolean  "own_email"
    t.boolean  "has_sale"
    t.boolean  "title_in_body"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "request"
    t.integer  "num"
  end

  create_table "requests", force: :cascade do |t|
    t.string   "phrase"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "urls", force: :cascade do |t|
    t.string   "path"
    t.integer  "url_length"
    t.integer  "slashes"
    t.integer  "title_chars"
    t.integer  "title_words"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
