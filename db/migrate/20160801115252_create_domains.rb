class CreateDomains < ActiveRecord::Migration[5.0]
  def change
    create_table :domains do |t|
      t.string :domain
      t.integer :domain_length
      t.boolean :consonants_m3
      t.integer :url_len_med
      t.integer :title_len_med
      t.integer :slashes_med
      t.integer :req_dom_pgs
      t.string :req_dom_url
      t.integer :direct_phones
      t.boolean :has800
      t.integer :total_phones
      t.boolean :has_about_page
      t.boolean :has_contact_page
      t.boolean :has_social
      t.integer :commun_methods
      t.boolean :own_email
      t.boolean :has_sale
      t.boolean :title_in_body
      t.references :request, index: true, foreign_key: true

      t.timestamps
    end
  end
end
