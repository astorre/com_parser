class AddRequestToDomain < ActiveRecord::Migration[5.0]
  def change
    add_column :domains, :request, :string
  end
end
