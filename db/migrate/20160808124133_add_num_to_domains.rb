class AddNumToDomains < ActiveRecord::Migration[5.0]
  def change
    add_column :domains, :num, :integer
  end
end
