class CreateUrls < ActiveRecord::Migration[5.0]
  def change
    create_table :urls do |t|
      t.string :path
      t.integer :url_length
      t.integer :slashes
      t.integer :title_chars
      t.integer :title_words
      t.references :domain, index: true, foreign_key: true

      t.timestamps
    end
  end
end
