class RemoveRequestFromDomains < ActiveRecord::Migration[5.0]
  def change
    remove_reference :domains, :request, foreign_key: true
  end
end
