class TasksController < ApplicationController

  def index
  end

  def create
    attachment = task_params[:attachment]
    File.open(Rails.root.join('public', 'uploads', attachment.original_filename), 'wb') do |file|
      file.write(attachment.read)
    end
    ImportWorker.perform_async attachment.original_filename
    respond_to do |format|
      format.html { render :index }
      format.js { render :create }
    end
  end

  def fill_up_titles
    TitleFetchWorker.perform_async
  end

  def export
    respond_to do |format|
      format.html
      format.xls { send_data xls_grouper(params[:id], @my_site_present, @grouping.requesting_id), filename: "#{Rails.root}/public/task_#{params[:id]}.xls" }
      format.csv { render_csv(params[:id]) }
    end
  end

  private

  def task_params
    params.require(:task).permit(:attachment)
  end

end
