class Domain_obj
  attr_reader :num,
              :request,
              :domain,
              :domain_length,
              :consonants_m3,
              :url_len_med,
              :title_len_med,
              :slashes_med,
              :req_dom_pgs,
              :req_dom_url,
              :direct_phones,
              :has800,
              :total_phones,
              :has_about_page,
              :has_contact_page,
              :has_social,
              :commun_methods,
              :own_email,
              :has_sale,
              :title_in_body

  def initialize(num, request, domain, domain_length, consonants_m3, url_len_med,
                title_len_med, slashes_med, req_dom_pgs, req_dom_url,
                direct_phones, has800, total_phones, has_about_page,
                has_contact_page, has_social, commun_methods, own_email,
                has_sale, title_in_body)
    @num              = num
    @request          = request
    @domain           = domain
    @domain_length    = domain_length
    @consonants_m3    = consonants_m3
    @url_len_med      = url_len_med
    @title_len_med    = title_len_med
    @slashes_med      = slashes_med
    @req_dom_pgs      = req_dom_pgs
    @req_dom_url      = req_dom_url
    @direct_phones    = direct_phones
    @has800           = has800
    @total_phones     = total_phones
    @has_about_page   = has_about_page
    @has_contact_page = has_contact_page
    @has_social       = has_social
    @commun_methods   = commun_methods
    @own_email        = own_email
    @has_sale         = has_sale
    @title_in_body    = title_in_body
  end
end

class DomainsController < ApplicationController
  def index
    respond_to do |format|
      format.html
      format.xls { send_data xls_grouper, filename: "#{Rails.root}/public/domains.xls" }
    end
  end

  def create
    attachment = task_params[:attachment]
    File.open(Rails.root.join('public', 'uploads', attachment.original_filename), 'wb') do |file|
      file.write(attachment.read)
    end
    ImportDomainsWorker.perform_async attachment.original_filename
    respond_to do |format|
      format.html { render :index }
      format.js { render :create }
    end
  end

  def fill_up_domains
    DomainCheckWorker.perform_async
  end

  def add_nums
    AddNumsWorker.perform_async
  end

  private

  def task_params
    params.require(:domain).permit(:attachment)
  end

  def xls_grouper
    domains = Domain.all.order(:request, :num)
    domains.to_xls(
        cell_format: {
          weight:           :bold,
          bottom_color:     :green,
          left:             :dotted,
          right:            :dashed,
          top:              :dotted,
          bottom:           :dashed,
          horizontal_align: :left,
          font: Spreadsheet::Font.new('Courier New', color: :blue)
        },
        header_format: {
          weight: :bold,
          color:  :black,
          right:  :dashed,
          bottom: :double,
          diagonal_color: :red,
          pattern_fg_color: :red,
          pattern_bg_color: :border,
          cross_down: true
        },
        column_width: { [:requests] => 40 },
        columns: [
          :num,
          :request,
          :domain,
          :domain_length,
          :consonants_m3,
          :url_len_med,
          :title_len_med,
          :slashes_med,
          :req_dom_pgs,
          :req_dom_url,
          :direct_phones,
          :has800,
          :total_phones,
          :has_about_page,
          :has_contact_page,
          :has_social,
          :commun_methods,
          :own_email,
          :has_sale,
          :title_in_body
        ],
        headers: [
          '№',
          'Фраза',
          'Домен',
          'Длина домена в символах',
          'Количество 3 согласных идущих подряд в названии домена',
          'Средняя длина (медиана) url в символах',
          'Средняя длина TITLE (медиана)',
          'Среднее количество слешей',
          'Количество страниц сайта по данному запросу [запрос site:domen.ru]',
          'URL сайта по этому запросу',
          'количестов прямых номеров телефона',
          'есть ли 8-800',
          'сколько всего номеров телефонов есть на странице (разные вариации)',
          'есть ли страница О КОМПАНИИ',
          'Есть ли страница КОНТАКТЫ',
          'есть ли кнопки соцсетей (ссылки на них) или плагины шаринга (tech.yandex.ru/share)',
          'кол-во способов связи (вытаскиваем со страницы: скайп, почта, обратный звонок. обратная связь, онлайн консультант, городской\мобильный телефон , несколько телефонов)',
          'почта на своем домене есть/нет',
          'Наличие слов СКИДКА АКЦИИ РАСПРОДАЖА SALE и других на странице в h1-h6 или ссылках',
          'Все слова в title встречаются хотя бы 1 раз на странице'
        ]
    ).force_encoding('UTF-8')
  end
end
