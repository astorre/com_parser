require 'parser'

class TitleWorker
  include Sidekiq::Worker
  include Parser
  sidekiq_options retry: false

  def perform(id)
    url = Url.find(id)
    complete_url = /^http/i.match(url.path) ? url.path : "http://#{url.path}".delete("\s\t")
    res_status, response = get_with_redirect(complete_url)
    if res_status == :ok
      html_status, html = get_html(response)
      if html_status == :ok
        title_chars = count_title_chars(html)
        title_words = count_title_words(html)
        url.update_attributes!(title_chars: title_chars, title_words: title_words)
      end
    end
  end

end
