require 'addressable/uri'

class ImportWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(filename)
    import(filename)
  end

  private

  def import(filename)
    spreadsheet = open_spreadsheet(filename)

    spreadsheet.each_row_streaming(offset: 1) do |row|
      path = row[0].to_s
      begin
        uri = Addressable::URI.parse(path)
      rescue => e
        puts "\nERROR!: #{e.to_s}\nURL: #{path}".freeze
      else
        domain = uri.host.gsub(/^www./, '')
        domain = Domain.create!(domain: domain, domain_length: domain.length)
        clean_path = path.gsub(/^http(s):\/\//, '').gsub(/^www./, '')
        Url.create!(
          domain_id: domain.id,
          path: clean_path,
          url_length: clean_path.length,
          slashes: clean_path.count('/')
        )
      end
    end
  end

  def open_spreadsheet(filename)
    path = Rails.root.join('public', 'uploads', filename)
    case File.extname(filename)
    when ".csv" then Roo::Csv.new(path, nil, :ignore)
    when ".xls" then Roo::Excel.new(path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(path, file_warning: :ignore)
    else raise "Unknown file type: #{filename}"
    end
  end

end
