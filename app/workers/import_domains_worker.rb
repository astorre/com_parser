class ImportDomainsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(filename)
    import(filename)
  end

  private

  def import(filename)
    spreadsheet = open_spreadsheet(filename)

    spreadsheet.each_row_streaming(offset: 1) do |row|
      request = row[1].to_s
      domain = row[2].to_s.gsub(/^www./, '')
      Domain.create!(domain: domain, request: request)
    end
  end

  def open_spreadsheet(filename)
    path = Rails.root.join('public', 'uploads', filename)
    case File.extname(filename)
    when ".csv" then Roo::Csv.new(path, nil, :ignore)
    when ".xls" then Roo::Excel.new(path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(path, file_warning: :ignore)
    else raise "Unknown file type: #{filename}"
    end
  end

end
