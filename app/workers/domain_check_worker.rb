class DomainCheckWorker
  include Sidekiq::Worker
  include Parser
  sidekiq_options retry: false

  def perform
    # "req_dom_url is null or updated_at < '2016-08-04'::date"
    domain_ids = Domain.where(domain_length: nil).pluck(:id)
    domain_ids.each do |id|
      DomainCheckUnitWorker.perform_async id
    end
  end
end
