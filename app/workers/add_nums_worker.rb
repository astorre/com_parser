class AddNumsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    spreadsheet = open_spreadsheet
    data = []
    spreadsheet.each_row_streaming(offset: 1) do |row|
      num = row[0].to_s.to_i
      req = row[1].to_s
      domain = row[2].to_s.gsub(/^www./, '')
      data << { num: num, req: req, domain: domain }
      # d = Domain.find_by(request: req, domain: domain)
      # puts d.request
      # puts d.domain
    end
    data.each do |dat|
      d = Domain.find_by(request: dat[:req], domain: dat[:domain])
      # puts d.request
      # puts d.domain
      d.update_attributes(num: dat[:num])
    end
    # puts data
  end

  private

  def open_spreadsheet
    filename = 'domains.xlsx'
    path = Rails.root.join('public', 'uploads', filename)
    case File.extname(filename)
    when ".csv" then Roo::Csv.new(path, nil, :ignore)
    when ".xls" then Roo::Excel.new(path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(path, file_warning: :ignore)
    else raise "Unknown file type: #{filename}"
    end
  end
end
