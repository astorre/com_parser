class TitleFetchWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    ids = Url.where(title_chars: nil).pluck(:id)
    total = ids.count
    ids.each_with_index do |id, i|
      puts "REMAIN #{total - i} sites"
      TitleWorker.perform_async id
    end
  end

end
