# encoding: utf-8
require 'parser'

class DomainCheckUnitWorker
  include Sidekiq::Worker
  include Parser
  sidekiq_options retry: false

  def perform(domain_id)
    d = Domain.find(domain_id)
    domain = d.domain
    request = d.request

    domain_length = domain.length
    consonants_m3 = domain.match(/[bcdfghj-np-tvwxz]{3}/i)
    urls = Url.where("path ~* ?", domain)#.where.not(title_chars: nil)
    url_len_med = urls.median(:url_length)
    title_len_med = urls.median(:title_chars)
    slashes_med = urls.median(:slashes)

    search_request = "#{request} site:#{domain}".freeze
    # puts "REQUEST: #{search_request}"
    document_paths = get_only_top(100, search_request, 213, false, 0)
    # puts "PATHS: #{document_paths}"

    req_dom_pgs = document_paths.length

    req_dom_url = document_paths[0]

    response_status, response = get_with_redirect(req_dom_url)

    if response_status == :ok
      html_status, html = get_html(response)

      if html_status == :ok
        body = clean_text(response)
        phone_numbers = phones(body)
        direct_phones = phone_numbers
          .count { |e| e =~ /(\+?)(7|8)\s*(\(*)(49(5|9))(\)*)\s*(\d{3})(-|\s*)(\d{2})(-|\s*)(\d{2})/ }
        has800 = phone_numbers
          .any? { |e| e =~ /(\+?)(7|8)\s*(\(*)(800)(\)*)\s*(\d{3})(-|\s*)(\d{2})(-|\s*)(\d{2})/ }

        has_about_page = has_about_page?(html)
        has_contact_page = has_contact_page?(html)

        has_social = has_social?(body)
        commun_methods = get_commun_methods(body, html)
        own_email = has_own_email?(html, req_dom_url)
        has_sale = has_sale?(html)
        title_in_body = has_title_in_body?(html)

        d.update_attributes!(
          domain_length:    domain_length,
          consonants_m3:    consonants_m3,
          url_len_med:      url_len_med,
          title_len_med:    title_len_med,
          slashes_med:      slashes_med,
          req_dom_pgs:      req_dom_pgs,
          req_dom_url:      req_dom_url,
          direct_phones:    direct_phones,
          has800:           has800,
          total_phones:     phone_numbers.length,
          has_about_page:   has_about_page,
          has_contact_page: has_contact_page,
          has_social:       has_social,
          commun_methods:   commun_methods,
          own_email:        own_email,
          has_sale:         has_sale,
          title_in_body:    title_in_body
        )
      end
    end
  end

  private

  def get_only_top(size, request, reg, reserve, page)

    puts "REQUEST: #{request}"

    xml = parse_xml(get_xml_url_top(size, CGI.escape(request), '', reg, reserve, page))
    if xml.nil?
      xml = parse_xml(get_xml_url_top(size, CGI.escape(request), '', reg, true, page))
    end

    hosts_extract(xml)

  end

  def parse_xml(url)

    status, respond = get_with_redirect(url)

    puts "status: #{status}".freeze if Rails.env.development?

    if status == :error
      puts "ERROR YA_XML_RESPONSE!".freeze
      return nil
    end

    Nokogiri::XML(respond, nil, 'UTF-8'.freeze)
  end

  def hosts_extract(xml)
    xml.xpath("//group//url".freeze)
    .map { |e| e.text.downcase }.freeze
  end

  def get_xml_url_top(size, query, site, reg, reserve, page)
    attributes = "&l10n=ru&sortby=rlv&filter=none&groupby=attr%3Dd.mode%3Ddeep.groups-on-page%3D#{size}.docs-in-group%3D1&page=#{page}".freeze
    "#{get_account(reserve)}&query=#{query}#{site}&lr=#{reg}#{attributes}".freeze
  end

  def get_account(reserve)
    ya_login              = Rails.application.secrets.ya_login
    ya_key                = Rails.application.secrets.ya_key
    xmlproxy_login        = Rails.application.secrets.xmlproxy_login
    xmlproxy_key          = Rails.application.secrets.xmlproxy_key
    xml_account           = "https://xmlsearch.yandex.ru/xmlsearch?user=#{ya_login}&key=#{ya_key}".freeze
    reserve_xml_account   = "http://xmlproxy.ru/xmlsearch?user=#{xmlproxy_login}&key=#{xmlproxy_key}".freeze

    reserve ? reserve_xml_account : xml_account
  end

end
