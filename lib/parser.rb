# encoding: utf-8

module Parser

  def get_html(response)
    [:ok, Nokogiri::HTML(response)].freeze
  rescue e
    puts "NOKOGIRI ERROR #{url}"
    [:error, nil].freeze
  end

  def count_title_chars(nokogiri_html_obj)
    title(nokogiri_html_obj).split(//).length
  end

  def count_title_words(nokogiri_html_obj)
    title(nokogiri_html_obj).split().length
  end

  def phones(body)
    body
      .to_enum(:scan,
        /(\+?)(7|8)\s*(\(*)(\d{3})(\)*)\s*(\d{3})(-|\s*)(\d{2})(-|\s*)(\d{2})/)
      .map {$&}.map { |e| e.gsub(/[\+\(\)\-\s]/, '') }.uniq
  rescue => e
    puts "PHONES error: #{e}"
    []
  end

  def has_about_page?(nokogiri_html_obj)
    nokogiri_html_obj.search('a').any? { |link| link['href'] =~ /about/i }
  end

  def has_contact_page?(nokogiri_html_obj)
    nokogiri_html_obj.search('a').any? { |link| link['href'] =~ /contact/i }
  end

  def has_social?(body)
    body.match(/share/i).present?
  end

  def get_commun_methods(body, nokogiri_html_obj)
    counter = 0
    counter += 1 if has_skype?(body)
    counter += 1 if has_email?(nokogiri_html_obj)
    counter += 1 if has_callback?(body)
    counter += 1 if has_online_cons?(body)
    counter += 1 if has_phone?(body)
    counter
  end

  def has_own_email?(nokogiri_html_obj, own_domain)
    nokogiri_html_obj.search('a').select { |link| link['href'] =~ /mailto/ }
      .any? { |e| e =~ /#{own_domain}/i }
  end

  def has_sale?(nokogiri_html_obj)
    nokogiri_html_obj.search('h1,h2,h3,h4,h5,h6,a').any? { |link| clean_text(link.text) =~ /скидк|акци|распродаж|sale/i }
  end

  def has_title_in_body?(nokogiri_html_obj)
    body = body(nokogiri_html_obj)
    title(nokogiri_html_obj).split.any? { |word| body =~ /#{word}/ }
  end

  def get_with_redirect(url, cookies: {})
    response = RestClient::Request.execute(method: :get, url: url, timeout: 10)
    result = response.to_str
  rescue => e
    puts "\nERROR!: #{e.to_s}\nURL: #{url}".freeze
    [:error, nil].freeze
  else
    case response.code
    when 301, 302, 303, 307
      cookie_string = response.headers["Set-Cookie"].freeze
      cookies = if cookie_string
        if cookie_string.is_a? Array
          cookie_string
          .map { |e| e.split(/;\s*/).map { |k| k.split('=', -1) } }
          .map { |l| Hash[l.map { |key, value| [key, value] }] }
          .reduce({}, :merge)
        else
          res = cookie_string.split(/;\s*/, 2)[0].split('=')
          { res[0] => res[1] }
        end
      elsif !cookies.empty?
        cookies
      else
        {}
      end
      get_with_redirect(response.headers["Location"].freeze, cookies: cookies)
    when 400, 404, 500
      puts "\nERROR STATUS!: #{response.code}\nURL: #{url}".freeze
      [:error, nil].freeze
    else
      [:ok, result].freeze
    end
  end

  def body(html)
    b = html.at('body')
    if b.present?
      # 'style,script,noscript,noframes,select,form,a,header,footer,aside,dialog,details,figcaption,figure,footer,header,menu,menuitem,meter,nav,progress,time,canvas,svg,audio,embed,source,track,video,address,frame,iframe,#header,#footer,#nav,#menu,.left_block'
      b.search('style'.freeze).remove
      clean_text(html.css('body').text)
    else
      ''#nil
    end
  end

  def has_skype?(body)
    body.match(/skype/i).present?
  end

  def has_email?(nokogiri_html_obj)
    nokogiri_html_obj.search('a').any? { |link| link['href'] =~ /mailto/ }
  end

  def has_callback?(body)
    body.match(/обратный звонок/i).present?
  end

  def has_online_cons?(body)
    body.match(/(jivosait|talk-me|me-talk|copiny|zopim|cleversite|redhelper|livetex|onicon)/i).present?
  end

  def has_phone?(body)
    body.match(/(\+?)(7|8)\s*(\(*)(\d{3})(\)*)\s*(\d{3})(-|\s*)(\d{2})(-|\s*)(\d{2})/).present?
  end

  def clean_text(str)
    s = str.encode('UTF-8', invalid: :replace, undef: :replace, replace: '')
    s
      .gsub(/[^а-яa-z0-9\-_\s.|!|?]/i, ' ')
      .gsub(/ё/, 'е')
      .gsub(/Ё/, 'Е')
      .downcase
      .strip
      .squeeze(' ')
      .gsub(/\s-+\s/, ' ').freeze
  end

  def title(html)
    title = clean_text(html.css('title').text)
    if title.nil?
      puts "TITLE IS NIL: #{@url}"
      ''
    else
      title
    end
  end

  def links(html)
    b = html.at('body')
    if b.present?
      l = []
      b.search('a').each do |e|
        c_t = clean_text(e.text)
        if c_t.present? && c_t !~ /^(\s|\u{a0})+$/
          l << c_t
        end
      end
      l
    else
      []#nil
    end
  end

  def parse_response(url)
    status, respond = get_with_redirect(url)
    if status == :error
      puts "Site #{url} unable to open."
      [:error, nil].freeze
    else
      [:ok, Nokogiri::HTML(respond)].freeze
    end
  end
end
