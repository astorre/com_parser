# dump the development db
#rake db:dump

# dump the production db
#RAILS_ENV=production rake db:dump

# dump the production db & restore it to the development db
#RAILS_ENV=production rake db:dump
#rake db:restore

# note: config/database.yml is used for database configuration,
#       but you will be prompted for the database user's password

namespace :db do

  desc 'Dumps the database to db/engine.dump'
  task :dump => :environment do
    cmd = nil
    with_config do |app, host, db, user|
      cmd = "pg_dump --host #{host} --username #{user} --verbose --clean --no-owner --no-acl --format=c #{db} > #{Rails.root}/db/#{app}.dump"
    end
    puts cmd
    exec cmd
  end

  desc 'Restores the database dump at db/neo_engine.dump'
  task :restore => :environment do
    cmd = nil
    with_config do |app, host, db, user|
      cmd = "pg_restore --verbose --host #{host} --username #{user} --clean --no-owner --no-acl --dbname #{db} #{Rails.root}/db/#{app}.dump"
    end
    Rake::Task['db:drop'].invoke
    Rake::Task['db:create'].invoke
    puts cmd
    exec cmd
  end

  desc 'Restores the database sql at db/neo_engine.sql'
  task import: :environment do
    import_path = "#{Rails.root}/db"
    sql_file = 'PostgreSQL.sql'
    database_config = Rails.configuration.database_configuration[Rails.env]

    # Unpack
    system "tar xf #{Rails.root}/db/engine_backup.tar -C #{import_path}"
    system "gzip -df #{import_path}/engine_backup/databases/#{sql_file}.gz"

    # Import
    system "psql --username=#{database_config['username']} -no-password #{database_config['database']} < #{import_path}/engine_backup/databases/#{sql_file}"
  end

  private

  def with_config
    yield Rails.application.class.parent_name.underscore,
        ActiveRecord::Base.connection_config[:host],
        ActiveRecord::Base.connection_config[:database],
        ActiveRecord::Base.connection_config[:username]
  end
end
